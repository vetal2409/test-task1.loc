<?php

namespace component;

use DateTime;
use Model\User;

class UserRatingManager
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return $this
     */
    public function byCountry()
    {
        switch ($this->user->country) {
            case 'Hungary':
                $this->user->rating += 2;
                break;
            case 'Germany':
                $this->user->rating += 3;
                break;
            case 'France':
                $this->user->rating += 4;
                break;
            case 'Russian Federation':
                $this->user->rating += 5;
                break;
            case 'Ukraine':
                $this->user->rating += 6;
                break;
            case 'Bulgaria':
                $this->user->rating += 7;
                break;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function byEven()
    {
        if ($this->user->id % 3 === 0) {
            $this->user->rating++;
        }
        if ($this->user->id % 4 === 0) {
            $this->user->rating += 2;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function byCreatedAt()
    {
        $date = new DateTime($this->user->created_at);
        $monthInt = (int)$date->format('m');
        switch (true) {
            case $monthInt <= 3:
                $this->user->rating *= 1;
                break;
            case $monthInt <= 6:
                $this->user->rating *= 2;
                break;
            case $monthInt <= 9:
                $this->user->rating *= 3;
                break;
            case $monthInt <= 12:
                $this->user->rating *= 4;
                break;
        }
        return $this;
    }

    /**
     * @param $averageRating
     * @return $this
     */
    public function byAverage($averageRating)
    {
        if ($this->user->rating > $averageRating) {
            $this->user->rating -= 5;
        }
        return $this;
    }
}
