<?php

namespace Model;

use Model;

class User extends Model
{

    public static $attributes = [
        'username',
        'password_hash',
        'first_name',
        'last_name',
        'country',
        'postcode',
        'status',
        'rating',
        'created_at',
    ];

    //TODO All property is not private to save time
    public $id;
    public $username;
    public $password_hash;
    public $first_name;
    public $last_name;
    public $country;
    public $postcode;
    public $status;
    public $rating;
    public $created_at;

    public static function tableName()
    {
        return 'user';
    }
}
