<?php

namespace Controller;

use component\UserRatingManager;
use HttpKernel\Controller\Controller;
use Model\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class User
 * @package Controller
 */
class UserController extends Controller
{
    public function indexAction()
    {
        $response = '';
//        $users = User::findAll();
        $users = User::find([], ['rating' => 'DESC']);

        $routeFixtures = $this->getUrlGenerator()->generate('fixture_index');
        $routeMigrations = $this->getUrlGenerator()->generate('migration_index');
        $routeRR = $this->getUrlGenerator()->generate('user_rating_recalculate');
        $response .= '<a href="' . $routeRR . '">Recalculate users rating</a><br>
                    <a href="' . $routeMigrations . '">Migrations<br></a>
                    <a href="' . $routeFixtures . '">Fixtures<br><br></a>';
        if (is_array($users) && count($users)) {
            foreach ($users as $user) {
                $response .= '<b>Rating: ' . (int)$user->rating . "</b> (Id: {$user->id}, Username: {$user->username}, 
                Password Hash: {$user->password_hash}, First Name: {$user->first_name}, Last Name: {$user->last_name},
                Country: {$user->country})<br>, Postcode: {$user->country}, Status: {$user->status},
                Created at: {$user->created_at}))<br><br><br>";
            }
        } else {
            $response .= 'There are not any record!';
        }

        return new Response($response);
    }

    public function ratingRecalculateAction()
    {
        /**
         * @var User[] $users
         */
        $users = User::findAll();
        $summaryRating = 0;
        $count = 0;
        foreach ($users as $user) {
            set_time_limit(30);
            $user->rating = null;
            $userRatingManager = new UserRatingManager($user);
            $userRatingManager->byCountry()->byEven()->byCreatedAt()->byAverage($count ? $summaryRating / $count : 0);
            $summaryRating += $user->rating;
            $count++;
            $user->save();
        }
        $routeUsers = $this->getUrlGenerator()->generate('user_index');
        return new RedirectResponse($routeUsers);
    }
}
