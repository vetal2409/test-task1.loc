<?php

namespace Controller;

use HttpKernel\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MigrationController extends Controller
{
    public function indexAction()
    {
        $routeMigrationUp = $this->getUrlGenerator()->generate('migration_up');
        $routeFixtures = $this->getUrlGenerator()->generate('fixture_index');
        $routeUsers = $this->getUrlGenerator()->generate('user_index');
        return new Response('<a href="' . $routeMigrationUp . '">Run Migration Up!!!</a><br>
                            <a href="' . $routeFixtures . '">Fixtures</a><br>
                            <a href="' . $routeUsers . '">Users</a>');
    }

    /**
     * Some piece of horrible and fast code which imitate the migrations
     * @throws \InvalidArgumentException
     */
    public function upAction()
    {
        $response = '';
        $conn = static::getMysqliConnection();

        if (!($stmt = $conn->prepare('SET FOREIGN_KEY_CHECKS=0'))) {
            $response .= 'Prepare failed: (' . $conn->errno . ') ' . $conn->error;
            return new Response($response);
        }
        if (!$stmt->execute()) {
            $response .= 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error;
            return new Response($response);
        }


        if (!($stmt = $conn->prepare('DROP TABLE IF EXISTS `user`'))) {
            $response .= 'Prepare failed: (' . $conn->errno . ') ' . $conn->error;
            return new Response($response);
        }
        if (!$stmt->execute()) {
            $response .= 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error;
            return new Response($response);
        }


        if (!($stmt = $conn->prepare("
            CREATE TABLE `user` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `postcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `status` tinyint(1) NOT NULL DEFAULT '0',
              `rating` int(11) DEFAULT NULL,
              `created_at` datetime NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;"))
        ) {
            $response .= 'Prepare failed: (' . $conn->errno . ') ' . $conn->error;
            return new Response($response);
        }

        if (!$stmt->execute()) {
            $response .= 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error;
            return new Response($response);
        }

        $response .= 'Done! <br>';
        $routeMigration = $this->getUrlGenerator()->generate('migration_index');
        $routeFixture = $this->getUrlGenerator()->generate('fixture_index');
        $response .= '<a href="' . $routeMigration . '">Migrations</a><br><a href="' . $routeFixture . '">Fixtures</a>';
        return new Response($response);
    }
}
