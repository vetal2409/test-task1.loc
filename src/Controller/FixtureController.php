<?php

namespace Controller;

use HttpKernel\Controller\Controller;
use Model\User;
use Symfony\Component\HttpFoundation\Response;
use Faker\Factory;

class FixtureController extends Controller
{
    public function indexAction()
    {
        $routeFixtureUser = $this->getUrlGenerator()->generate('fixture_user');
        $routeUsers = $this->getUrlGenerator()->generate('user_index');
        $response = '<a href="' . $routeFixtureUser . '">Run user fixture</a>
                    <br><a href="' . $routeUsers . '">Users</a>';
        return new Response($response);
    }

    public function userAction()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            set_time_limit(30);
            $user = new User();
            $user->username = $faker->userName;
            $user->password_hash = password_hash($faker->password(), PASSWORD_DEFAULT);
            $user->first_name = $faker->firstName;
            $user->last_name = $faker->lastName;
            $user->country = $faker->country;
            $user->postcode = $faker->postcode;
            $user->created_at = $faker
                ->dateTimeBetween('2015-01-01 00:00:00', '2015-12-31 00:00:00')
                ->format('Y-m-d H:i:s');
            $user->status = 1;
            $user->save();
        }
        $routeFixtures = $this->getUrlGenerator()->generate('fixture_index');
        $routeUsers = $this->getUrlGenerator()->generate('user_index');
        $response = 'Done!<br><br><a href="' . $routeFixtures . '">Fixtures</a>
                    <br><a href="' . $routeUsers . '">Users</a>';
        return new Response($response);
    }
}
