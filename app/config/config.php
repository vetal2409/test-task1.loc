<?php
return [
    'db' => [
        'host' => '127.0.0.1',
        'user' => 'root',
        'password' => '',
        'dbname' => 'test-task1',
        'port' => '3306',
        'charset' => 'UTF8',
    ]
];
