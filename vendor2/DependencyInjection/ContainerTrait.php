<?php

namespace DependencyInjection;

use mysqli;

trait ContainerTrait
{
    protected static function getContainer()
    {
        return Container::getInstance();
    }

    /**
     * @return mysqli object which represents the connection to a MySQL Server.
     */
    protected static function getMysqliConnection()
    {
        return static::getContainer()->get('mysqli');
    }
}
