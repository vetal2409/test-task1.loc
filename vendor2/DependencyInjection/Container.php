<?php

namespace DependencyInjection;

class Container
{
    protected static $instance;
    protected $services = array();

    protected function __construct()
    {
        //todo config class
        $config = require __DIR__ . '/../../app/config/config.php';


        $configDb = &$config['db'];
        $link = mysqli_connect($configDb['host'], $configDb['user'], $configDb['password'], $configDb['dbname']);

        if (!$link) {
            throw new \ErrorException('Mysqli connect failed');
        }

        $this->set('mysqli', $link);
    }

    /**
     * @return Container
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function get($id)
    {
        $id = strtolower($id);

        if (array_key_exists($id, $this->services)) {
            return $this->services[$id];
        }
        return null;
    }

    public function set($id, $service)
    {
        $id = strtolower($id);

        $this->services[$id] = $service;
    }
}
