<?php

namespace HttpKernel;

use Controller\FixtureController;
use Controller\MigrationController;
use Controller\UserController;
use DependencyInjection\Container;
use DependencyInjection\ContainerTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Kernel
 */
abstract class Kernel
{
    use ContainerTrait;

    /**
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\ResourceNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MethodNotAllowedException
     * @throws \InvalidArgumentException
     */
    public function handle(Request $request)
    {
        //Routing: hardcoded :) (only for fast test task)
        $routes = new RouteCollection();
        $routes->add('default', new Route('/', array('controller' => 'User')));
        $routes->add('migration_index', new Route('/migration', array('controller' => 'Migration')));
        $routes->add('migration_up', new Route('/migration/up', array('controller' => 'Migration')));
        $routes->add('fixture_index', new Route('/fixture', array('controller' => 'Fixture')));
        $routes->add('fixture_user', new Route('/fixture/user', array('controller' => 'Fixture')));
        $routes->add('user_index', new Route('/user', array('controller' => 'User')));
        $routes->add('user_rating_recalculate', new Route('/user/rating/recalculate', array('controller' => 'User')));


        $context = new RequestContext($request->getBaseUrl());
//        var_dump($context, $request->getBaseUrl()); exit;
        $matcher = new UrlMatcher($routes, $context);

        try {
            $parameters = $matcher->match($request->getPathInfo());
        } catch (\Exception $e) {
            throw new RouteNotFoundException($e->getMessage());
        }

        $container = Container::getInstance();
        $container->set('url-generator', new UrlGenerator($routes, $context));


        if ($parameters['controller'] === 'Migration') {
            $controller = new MigrationController();
            if ($parameters['_route'] === 'migration_index') {
                return $controller->indexAction();
            } elseif ($parameters['_route'] === 'migration_up') {
                return $controller->upAction();
            }
        }

        if ($parameters['controller'] === 'Fixture') {
            $controller = new FixtureController();
            if ($parameters['_route'] === 'fixture_index') {
                return $controller->indexAction();
            } elseif ($parameters['_route'] === 'fixture_user') {
                return $controller->userAction();
            }
        }

        if ($parameters['controller'] === 'User') {
            $controller = new UserController();
            if (in_array($parameters['_route'], ['default', 'user_index'], true)) {
                return $controller->indexAction();
            } elseif ($parameters['_route'] === 'user_rating_recalculate') {
                return $controller->ratingRecalculateAction();
            }
        }

        throw new RouteNotFoundException();
    }
}
