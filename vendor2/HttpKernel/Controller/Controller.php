<?php

namespace HttpKernel\Controller;

use DependencyInjection\ContainerTrait;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * Class Controller
 * @package Controller
 */
abstract class Controller
{
    use ContainerTrait;

    /**
     * @return UrlGenerator
     */
    protected function getUrlGenerator()
    {
        return static::getContainer()->get('url-generator');
    }
}
