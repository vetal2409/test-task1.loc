<?php

namespace AR;

use DependencyInjection\ContainerTrait;
use ErrorException;

abstract class ActiveRecord
{
    use ContainerTrait;

    protected static $attributes = [];

    /**
     * @return string
     */
    public static function tableName()
    {
        return '';
    }

    /**
     * @return array
     */
    public static function attributes()
    {
        return static::$attributes;
    }

    protected static function load(array $row)
    {
        $object = new static();
        foreach ($row as $attribute => $value) {
            $object->{$attribute} = $value;
        }
        return $object;
    }

    public static function findAll()
    {
        $mysqli = static::getMysqliConnection();
        if (!($stmt = $mysqli->prepare('SELECT * FROM ' . static::tableName()))) {
            return null;
        }
        $stmt->execute();
        $res = $stmt->get_result();
        $result = [];
        while ($row = $res->fetch_assoc()) {
            $result[] = static::load($row);
        }
        return $result;
    }

    public static function find($condition, $sort = [])
    {
        $mysqli = static::getMysqliConnection();
        $sqlSort = '';
        if (count($sort)) {
            foreach ($sort as $attr => $destination) {
                if (in_array(strtolower($destination), ['asc', 'desc'], true)) {
                    $sqlSort = "$attr $destination";
                }
            }
        }
        if ($sqlSort) {
            $sqlSort = ' ORDER BY ' . $sqlSort;
        }
        if (!($stmt = $mysqli->prepare('SELECT * FROM ' . static::tableName() . $sqlSort))) {
            throw new ErrorException('Prepare failed: (' . $mysqli->errno . ') ' . $mysqli->error);
        }
        $stmt->execute();
        $res = $stmt->get_result();
        $result = [];
        while ($row = $res->fetch_assoc()) {
            $result[] = static::load($row);
        }
        return $result;
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function save()
    {
        $attributes = static::attributes();
        $count = count($attributes);
        if (!$count) {
            return false;
        }
        $conn = static::getMysqliConnection();
        $primaryKey = 'id';
        if ($this->{$primaryKey}) {
            $updatePair = [];
            foreach ($attributes as $attribute) {
                $updatePair[] = $attribute . '=?';
            }
            $sql = 'UPDATE ' . static::tableName() . ' SET ' . implode(',', $updatePair)
                . ' WHERE `id` = ' . $this->{$primaryKey};
        } else {
            $sqlAttributes = implode(',', $attributes);
            $sqlValues = implode(',', array_fill(0, $count, '?'));
            $sql = 'INSERT INTO ' . static::tableName() . " ($sqlAttributes) VALUES ($sqlValues)";
        }
        if (!($stmt = $conn->prepare($sql))) {
            throw new ErrorException('Prepare failed: (' . $conn->errno . ') ' . $conn->error);
        }

        $params = [''];
        foreach ($attributes as $attribute) {
            $params[0] .= 's';
            $params[] = &$this->{$attribute};
        }
        call_user_func_array(array($stmt, 'bind_param'), $params);
        if (!$stmt->execute()) {
            throw new ErrorException('Execute failed: (' . $stmt->errno . ') ' . $stmt->error);
        }
        return true;
    }
}
