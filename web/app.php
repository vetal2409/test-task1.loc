<?php
use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/../vendor/autoload.php';
$request = Request::createFromGlobals();

$kernel = new AppKernel();
$response = $kernel->handle($request);
$response->send();
